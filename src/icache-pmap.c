if( obj->hclass == cache.hclass ) {
   val = obj->elements[ cache.index ];
} else if( obj->hclass == cache.pclass ) {
   // test the "prototype class" for a prototype cache hit; the property
   // is found in the prototype chain
   val = cache.owner->elements[ cache.index ];
} else {
   val = cacheReadMiss( obj, &cache );
}
