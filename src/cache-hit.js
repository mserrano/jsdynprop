"use strict";

function readX( obj ) { return obj.x; }

function test( count, N ) {
   let os = [{x: 21, y: 31}, ..., {x: 12, y: 123}];
   let s = 0;

   for( let j = 0; j < count; j++ ) {
      if( j % 1000 == 0 ) console.log( j );
   for( let i = 0; i < count; i++ ) {
      let o = os[ i % N ];
      s = readX( o );
   }
}

   return s;
}

const N = 1;
console.log( "N=", N );

console.log( test( 40000, N ) );
