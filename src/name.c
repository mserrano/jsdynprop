rope *stringToName( rope *prop ) {
  if( prop->name ) {
     return prop->name;
  } else {
     long hash = stringHash( prop );
     rope *name = globalNameTableGet( hash, prop );
     if( !name ) name = globalNameTablePut( hash, prop );
     prop->name = name;
     return name;
  }
}
