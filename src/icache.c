(obj->hclass == cache.hclass 
   ? obj->elements[ cache.index ]; // cache hit
   : cacheReadMiss( obj, "prop", &cache )) // cache miss
