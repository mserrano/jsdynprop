var handler = {
    get: function(obj, prop) {
        return prop in obj ? obj[prop] : 37;
    }
};
var p = new Proxy({}, handler);
p.a = 1;
console.log(p.a, p.b);
