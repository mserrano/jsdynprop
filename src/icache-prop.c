if( obj->hclass == cache.hclass ) {
   val = obj->elements[ cache.index ];
} else if( obj->hclass == cache.aclass ) {
   // test the "accessor class" for an accessor cache hit
   val = cache.owner->elements[ cache.index ]( obj );
} else {
   val = cacheReadMiss( obj, &cache );
}
