(( obj->hclass == cache.hclass
   && stringToName( prop ) == cache.prop ) 
      ? obj->elements[ cache.index ]; // cache hit
      : cacheReadMiss( obj, prop, &cache )) // cache miss

