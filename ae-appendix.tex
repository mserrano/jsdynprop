\section{Artifact Appendix}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Abstract}

The artifact associated with the submission \emph{Dynamic Property
Caches} is the Hop JavaScript static native compiler and the test
suite that is described and used in the paper. The compiler and the
tests are packaged in a way that facilitates the reproduction of the
experimental results of the paper. The artifact contains two automatic
procedures. One that re-runs the portion of the performance evaluation for
Hop and V8 and one that re-runs the full experiment. 

\noindent The source code of the compiler is contained in the directory:

\begin{lstlisting}[language=bash]
hop/hopc
\end{lstlisting}

\noindent The runtime support is located in the directory:

\begin{lstlisting}[language=bash]
hop/hopscript
\end{lstlisting}

\noindent The most significant files regarding the implementation
of proxies are:

\begin{lstlisting}[language=bash]
# proxy base class
hop/hopscript/proxy.scm
# the implementation of JavaScript properties
# and hidden classes
hop/hopscript/property.scm
# inline caches
hop/hopscript/property_expander.sch
# function calls
hop/hopscript/public.scm
\end{lstlisting}


\noindent The tests are located in the directory:

\begin{lstlisting}[language=bash]
jsdynprop/bench
\end{lstlisting}

\subsection{Artifact check-list (meta-information)}

{\small
\begin{itemize}
  \item {\bf Program: } The benchmarks we use are included in the submission.
  \item {\bf Compilation: } We present a performance evaluation for the
  Hop JavaScript static compiler, version 3.3.0. It requires the Scheme
  Bigloo compiler version 4.3f. The shell scripts we provide handle its
  installation.
  \item {\bf Run-time environment: } The artifact is not OS depend but it
  is developed and tested mainly under Linux.
  \item {\bf Hardware: } The performance evaluation assumes an \texttt{x86\_64}
  platform with at least four cores.
  \item {\bf Execution: } We expect that on common, modern computers (in 2019),
  the overall execution last about an hour for the simplified test and in
  between 30 to 60 hours for the full test.
  \item {\bf Metrics: } The metrics that are presented are the execution speed
  of the compiled tests.
  \item {\bf Output: } The output is a LaTeX table that is displayed on the
  console.
  \item {\bf Publicly available?: } The Hopc compiler is publicly available.
  \item {\bf Code licenses (if publicly available)?: } Free for academics.
\end{itemize}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Delivery}

The material needed to access, build, and run the artifact is available
at:
\begin{center}
  \url{https://gitlab.inria.fr/mserrano/jsdynprop/blob/master}
\end{center}
\noindent
including the source for this document in \texttt{ae-appendix.tex} so you do not need to copy
and paste from the PDF.

For the artifact evaluation we propose two methods: a native
installation and a docker installation. As much as possible, we
recommend using the native installation on a Linux Debian x86\_64
platform (equipped with a complete development kit) as it will provide
reliable results. The easiest solution to reproduce the experiment (but
with a loss of accuracy due to emulation) is to use docker. Normally,
it should be possible to compile and run the tests on MacOS X and
Windows but we did not test that environment.

The \texttt{README.md} file of the main directory explains how to
compile and run it. The result of the execution is the LaTeX tabular
that contains the speed measures for Hop and V8.


\paragraph{Hardware dependencies}
Hop runs on x86, x86\_64, and arm processor but the evaluation should be
conducted under x86\_64, as it is the platform that receives the best
support.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Installation}

This section describes the two ways to install the required
software. The first (in \ref{sec:installinux}) discusses how to
install on your own machine and requires some familiarity with linux
installations and software building. A native installation will
produce more reproducible results. The second (in
\ref{sec:installdocker}) uses docker and is very easy to get going,
once docker is intalled. This method of installation may not give
exactly the same measurements as reported in the paper because of the
overhead of emulating the operating system.

\subsubsection{Native Linux Installation}\label{sec:installinux}

A complete development kit is needed to compile the compiler. Assuming
a Linux Debian distribution, it can be installed with:

\begin{lstlisting}[language=bash]
sudo apt-get -qq update
sudo apt-get install -y dh-make
sudo apt-get install -y libssl1.0.2
sudo apt-get install -y libssl-dev
sudo apt-get install -y libsqlite3-0
sudo apt-get install -y libsqlite3-dev
sudo apt-get install -y libasound2
sudo apt-get install -y libasound2-dev
sudo apt-get install -y libflac8
sudo apt-get install -y libflac-dev
sudo apt-get install -y libmpg123-0
sudo apt-get install -y libmpg123-dev
sudo apt-get install -y libavahi-core7
sudo apt-get install -y libavahi-core-dev
sudo apt-get install -y libavahi-common-dev
sudo apt-get install -y libavahi-common3
sudo apt-get install -y libavahi-client3
sudo apt-get install -y libavahi-client-dev
sudo apt-get install -y libunistring0
sudo apt-get install -y libunistring-dev
sudo apt-get install -y libpulse-dev
sudo apt-get install -y libpulse0
sudo apt-get install -y automake
sudo apt-get install -y libtool
sudo apt-get install -y libgmp-dev
sudo apt-get install -y libgmp3-dev
sudo apt-get install -y libgmp10
\end{lstlisting}

\paragraph{Installing Bigloo}

In addition, the latest version of the Bigloo Scheme compiler is needed
to compile Hop. It can be obtained and installed with

\begin{lstlisting}[language=bash]
PREFIX=/tmp/HOP
biglootgz=bigloo-unstable.tar.gz

wget ftp://ftp-sop.inria.fr/indes/fp/Bigloo/$biglootgz \
  -O /tmp/$biglootgz
bigloo=`tar tfz /tmp/$biglootgz | head -n 1`
pushd /tmp \
  && rm -rf $bigloo \
  && tar xvfz /tmp/$biglootgz \
  && popd
(pushd /tmp/$bigloo \
  && ./configure --prefix=$PREFIX \
  && make -j 2 \
  && make install \
  && popd) || exit 1
\end{lstlisting}

\paragraph{Installing Hop}

Under Linux, Hop can be installed with
\begin{lstlisting}[language=bash]
PREFIX=/tmp/HOP
PATH=$PREFIX/bin:$PATH
export PATH

git clone https://github.com/manuel-serrano/hop
(pushd hop \
  && git checkout $COMMIT && \
  ./configure --prefix=$PREFIX \
    --disable-doc --license=academic \
    --bigloo=$PREFIX/bin/bigloo \
  && make \
  && make install) || exit 1
\end{lstlisting}


\subsubsection{Docker Installation}\label{sec:installdocker}

Using the \texttt{Dockerfile} provided below (and also on gitlab), proceed as follows:

\begin{lstlisting}[language=bash]
# build the docker image
docker build -t jsdynprop .
# run all the tests
docker run jsdynprop /tmp/jsdynprop/AE.sh
# start a shell in the context of the installation
docker run -t -i --entrypoint=/bin/bash jsdynprop
\end{lstlisting}

\noindent The full tests presented in the paper can be executed with:

\begin{lstlisting}[language=bash]
# run all the full tests
docker run jsdynprop /tmp/jsdynprop/AE.sh full
\end{lstlisting}

Depending on the computer, this will run for about
\textbf{30 to 60 hours} of intensive computing.

~

\hrule
\begin{scriptsize}\tt
\lstinputlisting[language=bash, numbers=none,basicstyle=\scriptsize]{docker/Dockerfile}
\end{scriptsize}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Experiment workflow}

The workflow of this experiment consists in first compiling the Hop
compiler. Then, in a second time, for all the tests, successively,
compile them with Hop and collected their execution times with V8
and Hop. All these results are collected and gathered inside a
single LaTeX table and a PDF barchart.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Evaluation and expected result}

The LaTeX table will contain values for Hop and V8 execution times.
The ration between these two systems should be conform to the
description of the paper. The barchart contains the same information
but presented graphically.

\noindent The PDF file can be extracted with:

\begin{lstlisting}[language=bash]
name=`docker ps -a --format "{{.Image}} {{.Names}}"`
name=`echo $name | grep jsdynprop`
name=`echo $name | head -n 1 | awk '{print $2}'`
docker cp $name:/tmp/jsdynprop/plain.pdf .
\end{lstlisting}

