%*=====================================================================*/
%*    serrano/diffusion/article/jsdynprop/2-strings.tex                */
%*    -------------------------------------------------------------    */
%*    Author      :  Manuel Serrano                                    */
%*    Creation    :  Wed Aug  7 12:18:12 2019                          */
%*    Last change :  Thu Jan  9 17:17:48 2020 (serrano)                */
%*    Copyright   :  2019-20 Manuel Serrano                            */
%*    -------------------------------------------------------------    */
%*    String implementation                                            */
%*=====================================================================*/

%*---------------------------------------------------------------------*/
%*    strings                                                          */
%*---------------------------------------------------------------------*/
\section{Strings}
\label{strings}

\js has two kinds of strings, objects created with the \TT{String}
constructor and \emph{string values}. The \TT{String} constructor is
actually mostly used as a container for the builtin string methods. It
is seldom used in actual programs. In contrast, string values
are ubiquitous, in particular because they are used to name object
properties. In the rest of this section we focus on string values.

\subsection{String Values}

\js string values are sequences of 16-bit unsigned integers. They are
neither UCS-2 nor UTF-16 strings as the \TT{charAt} method does not
interpret characters according to the UCS-2 code points and because
there is no interpretation of characters whose encoding spans over two
code units. The \TT{length} property of a string value
is specified not to give the number of UTF-16 characters composing
that string, but the number of 16-bit unsigned integers it
contains. Giving up on UTF-16 strings enables a linear access to
string characters so it might be sufficient for a \js implementation
to support strings as specified with UTF-16 code units. However, for
fast interfacing with the operating system and for compactness, it
might be sensible to distinguish between strings that can be encoded
as 8-bit unsigned integers and those that require 16-bit wide
integers. This is what \hop does: ASCII strings are encoded as sequences
of 8-bit integers and all other strings are encoded using the UTF-8
schema, which enables a fast interface with the operating system and
with foreign languages like Python that also use UTF-8
\iflongversion
strings~\footnote{An extension of our own is used to encode 16-bit
integer values that form illegal UTF-8 sequences.}. 
\else
strings.
\fi
Most other implementations also use the same dichotomy between 8-bit
and 16-bit encoding but generally they use 16-bit integers for
non-ASCII strings.

\js string values are constructed when string literals appear in the
program text, by \TT{String} methods such as \TT{charAt}, by regular
expression matching, and by concatenation (which is used extensively in
\js programs). The Mozilla Developer Network page dedicated to
strings~\cite{mdn:string} even suggests using the `\TT{+}' operator
to split long literals. As a consequence, fast \js implementations use
ropes as the underlying data structure for strings to support
fast concatenation~\cite{ropes:spe95, wikipedia:ropes}.

\begin{figure}[htb]
\centerline{\includegraphics[width=0.50\linewidth]{./svg/ropes.pdf}}
\caption{The \hop class hierarchy for \js string values. The \TT{left} and \TT{right}
pointers for ASCII strings point only to other ASCII strings. The \TT{left} and
\TT{right} pointers of UTF-8 strings point to arbitrary string ropes.
\label{ropes-fig}}
\end{figure}

The \hop base class for ropes consists of: a \TT{weight}, a \TT{left}
pointer (which is a sequence of characters if the node is a leaf and a
node otherwise), and a \TT{right} pointer that might be \TT{null}. A
subclass is used to represent ASCII strings. Another subclass is used
to represent UTF-8 strings (see Figure~\ref{ropes-fig}); the UTF-8
subclass has extra fields to support various unicode optimizations not
explained here.

\subsection{Property Names}

String values are used to name object properties and the property
lookup along an object prototype chain compares string
values. To optimize this operation, we modified \hop to store string
values used to name properties in a global table and implement the
property name comparison as a pointer-equality test. More precisely,
each string is augmented with an additional \TT{name} field that
points to its corresponding unique name. In the evaluation of
\TT{obj[prop]}, as described in Section~\ref{intro}, after \TT{prop}
is converted into a string value, its associated name is retrieved and
used in the operations that follow. The name retrieval is implemented
as follows:

\lstinputlisting[numbers=left, xleftmargin=0.38cm, language=C]{src/name.c}

In spite of its apparent complexity, this function is generally
extremely fast. The test \TT{if( prop->name )} succeeds most of
the time because, first, string literals are statically allocated as
names, \emph{i.e.,} their \TT{name} field points to themselves, so any
property name that is explicitly mentioned in the source code does not
require any runtime allocation. Second, as soon as a string is used in
any property operation, its associated name is allocated once and for all
(if it does not already exist) and stored in the string for future
use.

