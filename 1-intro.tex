%*=====================================================================*/
%*    serrano/diffusion/article/jsdynprop/1-intro.tex                  */
%*    -------------------------------------------------------------    */
%*    Author      :  Manuel Serrano                                    */
%*    Creation    :  Wed Aug  7 09:23:59 2019                          */
%*    Last change :  Thu Jan  9 16:59:05 2020 (serrano)                */
%*    Copyright   :  2019-20 Manuel Serrano                            */
%*    -------------------------------------------------------------    */
%*    Introduction                                                     */
%*=====================================================================*/

%*---------------------------------------------------------------------*/
%*    Introduction                                                     */
%*---------------------------------------------------------------------*/
\section{Introduction}
\label{intro}

\js object properties are referred to by their names, which are
strings.  The canonical syntactic form for property read and property
write is \TT{\textit{obj}[\textit{prop}]}, where both
\TT{\textit{obj}} and \TT{\textit{prop}} are expressions. When the
string forming the property name is syntactically well formed as an
identifier, the access can be abbreviated \TT{obj.prop} (where \TT{prop} is not
an evaluated expression when using the dot notation). The semantics
of object accesses is precisely defined in the language
specification~\cite{ecmascript5} in an algorithmic manner. For
evaluating an expression \TT{\textit{obj}[\textit{prop}]} the
following steps are executed:

\begin{enumerate}
\item evaluate \TT{obj} and convert it into an object if needed;
\item evaluate \TT{prop} and convert it into a string if needed;
\item look up the property in the object and, if not found,
  repeat the search along the object's prototype chain.
\item produce \TT{undefined} if the property's value is not found.
\end{enumerate}

These steps also apply to array accesses. For instance,
assuming that \TT{a} is an array, evaluating the expression
\TT{a[10]} requires converting the number \TT{10} into the string
\TT{"10"} and then looking for a property of that name in \TT{a} (and
possibly its prototype chain). For the sake of portability, the string
conversion is precisely defined. For instance, the specification gives
the number of decimal digits that must be taken into account for the
number conversion.

All fast \js implementations work hard to avoid executing the
steps in the semantics literally. For instance, when accessing an array they
avoid the number-to-string conversion as much as possible and for accessing an
object they use the well known technique of \emph{hidden classes} and
\emph{inline caches} to eliminate the lookup in the object and its
prototype
chain~\cite{Deutsch:1984:EIS:800017.800542,self:pldi89,Chambers:1989:EIS:74877.74884}.
Many descriptions of these techniques are
available~\cite{thompson:design-elements,v8,artoul:under-the-hood,Deutsch:1984:EIS:800017.800542,v8-fast-props}
so there is no need for yet another comprehensive description
here. For completeness sake, however, we include an excerpt of
\citet{sf:cc19}'s description that shows the basic technique
to help present this paper's optimization.

A property access \TT{obj.prop} or \TT{obj["prop"]} can be
implemented as follows, using C as the implementation language:

\lstinputlisting[numbers=none,language=C]{src/icache.c}

\noindent On a cache miss, the \TT{cache}'s {\texttt{hclass}}
attribute is updated with the object's hidden class. The object's hidden
class is updated each time a property is added or removed so the
condition {\texttt{obj->hclass}} {\texttt{==}}
{\texttt{cache->hclass}} holds only for objects that have exactly the
same structure. Compared to a structure field access \TT{obj->prop} in
C, the overhead of a cache hit is three memory reads
({\texttt{obj->hclass}}, {\texttt{cache.hclass}}, and
{\texttt{cache.index}}) and one comparison. All fast \js
implementation use similar sequences for accessing object
properties.

Various improvements to inline caching have been studied (notably
``polymorphic inline caching''~\cite{Holzle:1991,sf:cc19}), but
these techniques assume that the
property names themselves are invariant. This limitation is visible in the code
above, as it has a single use of \TT{"prop"} in the cache-miss code.
Optimizing situations where the name is dynamic are the subject of this study.

Dynamic property names are not as common as static ones, so the optimization
we propose in this paper is not as critical as the inline cache
optimization. That said, dynamic property names are used by the \TT{for..in}
construct (a construct that dates to the first version of \js) and,
more importantly, they are used extensively with proxy objects
(introduced in ECMAScript 6~\cite{ecmascript6}). While both are important, improving the
performance of proxy object is the main motivation for this work.

A proxy object~\cite{custem:dls10,Cutsem2013TrustworthyP} encapsulates
another object and interposes on the primitive operations it supports:
property reads and writes, function calls, property deletion,
etc. Proxy objects serve several purposes including security
enforcement~\cite{keil:ecoop2015}, higher order
contracts~\cite{chaperones}, and gradual
typing~\cite{Rastogi:2015:SEG:2676726.2676971}. Unfortunately, the
design of proxies makes it difficult to implement them efficiently,
that is, with performance comparable to those of plain objects.  We
have conducted a performance evaluation that shows that the
performance penalty imposed by proxies on current, efficient commercial
\js implementations is a slowdown of about one or two orders of
magnitude, which disqualifies proxies from extensive use.

In this paper, we propose several optimizations that reduce the
slowdown significantly. In general, the optimizations we offer in this
paper are not sophisticated; their value is that we have identified a
set of optimizations that are both easy to implement and are
effective.

We have implemented all of them in \hop, an ahead-of-time
\js compiler~\cite{serrano:dls18}. With these
modifications, \hop outperforms all other implementations when proxies
are used. On average it reduces the slowdown to at most
\ratioProxyHop. This is probably not enough yet for extensive use of
proxy objects but this is a first step in the direction of increasing
proxy usability and likely makes the difference in some applications.

The paper is organized as follows. In Section~\ref{strings}, we first
present the string implementation, a central subsystem in the
implementation of dynamic property accesses. In Section~\ref{dynprop},
we show how we optimize dynamic property accesses. In
Section~\ref{proxy} we present how we accommodate the inline cache
implementation to minimize the performance penalty of proxy objects.
In Section~\ref{perfs} we present the evaluation report. In
Section~\ref{related} we present the related work and we conclude in
Section~\ref{conclusion}.

