To build the docker image:

  docker build -t jsdynprop .

To run it:

  docker run jsdynprop /tmp/jsdynprop/AE.sh
  
To run the full test (30 to 60 hours) it:

  docker run jsdynprop /tmp/jsdynprop/AE.sh full
