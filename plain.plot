set terminal pdf font "Verdana,8"
set output 'plain.pdf'

set title 'Plain performance'
set ylabel "execution time (in sec)" offset 5,0

set auto x

set style data histogram
set style histogram cluster gap 1
set xtics rotate by 45 right

set boxwidth 0.9
set style fill solid
set style line 1 linecolor rgb '#3264c8' linetype 1 linewidth 1
set style line 2 linecolor rgb '#d83812' linetype 1 linewidth 1
set style line 3 linecolor rgb '#fa9600' linetype 1 linewidth 1
set style line 4 linecolor rgb '#109318' linetype 1 linewidth 1
set style line 5 linecolor rgb '#960096' linetype 1 linewidth 1

set grid ytics
set xtics scale 0
set datafile separator ","

set lmargin 6
set rmargin 1
set key under nobox
set logscale y

plot \
  'plain.csv' u 2:xtic(1) title 'graal' ls 1, \
  'plain.csv' u 3:xtic(1) title 'hop' ls 2, \
  'plain.csv' u 4:xtic(1) title 'jsc' ls 3, \
  'plain.csv' u 5:xtic(1) title 'js60' ls 4, \
  'plain.csv' u 6:xtic(1) title 'v8' ls 5

