set terminal pdf font "Verdana,18"
set output 'sieve.pdf'

set title 'Compared performance of property accesses'
set ylabel "execution time (in sec)" 

set auto x

set style data histogram
set style histogram cluster gap 1
set xtics 
set boxwidth 0.9
set style fill solid
set style line 1 linecolor rgb '#3264c8' linetype 1 linewidth 1
set style line 2 linecolor rgb '#d83812' linetype 1 linewidth 1
set style line 3 linecolor rgb '#fa9600' linetype 1 linewidth 1
set style line 4 linecolor rgb '#109318' linetype 1 linewidth 1
set style line 5 linecolor rgb '#960096' linetype 1 linewidth 1

set grid ytics
set xtics scale 0
set datafile separator ","

set key under nobox

plot \
  'sieve.csv' u 2:xtic(1) title 'graal' ls 1, \
  'sieve.csv' u 3:xtic(1) title 'hop' ls 2, \
  'sieve.csv' u 4:xtic(1) title 'jsc' ls 3, \
  'sieve.csv' u 5:xtic(1) title 'js60' ls 4, \
  'sieve.csv' u 6:xtic(1) title 'v8' ls 5

