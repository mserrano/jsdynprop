set terminal pdf font "Verdana,6"
set output 'proxy.pdf'

set title 'Proxy performance'
set ylabel "execution time (in sec)" offset 0,0

set auto x

set style data histogram
set style histogram gap 1 errorbars lw 1
set errorbars lc rgb '#444444'
set xtics rotate by 45 right

set boxwidth 0.9
set style fill solid
set style line 1 linecolor rgb '#3264c8' linetype 1 linewidth 1
set style line 2 linecolor rgb '#d83812' linetype 1 linewidth 1
set style line 3 linecolor rgb '#fa9600' linetype 1 linewidth 1
set style line 4 linecolor rgb '#109318' linetype 1 linewidth 1
set style line 5 linecolor rgb '#960096' linetype 1 linewidth 1

set grid ytics
set xtics scale 0
set datafile separator ","

set lmargin 6
set rmargin 1
set key under nobox
set logscale y

plot \
  'proxy.csv' u 2:3:4:xtic(1) title 'graal' ls 1 , \
  'proxy.csv' u 5:6:7:xtic(1) title 'hop' ls 2 , \
  'proxy.csv' u 8:9:10:xtic(1) title 'jsc' ls 3 , \
  'proxy.csv' u 11:12:13:xtic(1) title 'js60' ls 4 , \
  'proxy.csv' u 14:15:16:xtic(1) title 'v8' ls 5 


