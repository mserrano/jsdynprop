#!/bin/bash

echo "Entering /tmp/jsdynprop...$2"
cd /tmp/jsdynprop

engines="nodejs hop"
allengines="-e bench/engines/hop.json -e bench/engines/nodejs.json"
iteration=3

if [ "$2 " = "full " ]; then
  engines="nodejs hop jsc js60 graal"
  allengines="-e bench/engines/hop.json -e bench/engines/nodejs.json -e bench/engines/jsc.json -e bench/engines/js60.json -e bench/engines/graal.json"
  iteration=30
fi

echo "Running $engines tests..."
(cd bench; make cleanall)
(cd bench; make all-sans-mem JSBENCHDIR=/tmp/jsbench/tools ENGINES="$engines" ITERATION=$iteration HOPDEPENDENCIES=)

echo "Generating LaTeX tabular..."
rm -f proxy-plain.tex
make proxy-plain.tex JSBENCHDIR=/tmp/jsbench/tools ALLENGINES="$allengines"

echo "Generating PDF figure..."
rm -f plain.pdf plain.csv plain.plot
make plain.pdf JSBENCHDIR=/tmp/jsbench/tools ALLENGINES="$allengines"

echo ""
cat proxy-plain.tex

echo "See also plain.pdf"
echo "Instructions for getting plain.pdf:"
echo "  1. get the name of the running docker container:"
echo "     name=\`docker ps -a --format \"{{.Image}} {{.Names}}\"  | grep jsdynprop | head -n 1 | awk '{print \$2}'\`"
echo "  2. copy the file from that container"
echo "     docker cp \$name:/tmp/jsdynprop/plain.pdf ."


