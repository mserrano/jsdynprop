#*=====================================================================*/
#*    serrano/diffusion/article/jsdynprop/Makefile                     */
#*    -------------------------------------------------------------    */
#*    Author      :  Manuel Serrano                                    */
#*    Creation    :  Wed May 26 15:17:40 2010                          */
#*    Last change :  Wed Jan 22 11:55:15 2020 (serrano)                */
#*    Copyright   :  2010-20 Manuel Serrano                            */
#*    -------------------------------------------------------------    */
#*    HopTeX makefile to build a paper                                 */
#*=====================================================================*/

#*---------------------------------------------------------------------*/
#*    The tools, compilers, and flags.                                 */
#*---------------------------------------------------------------------*/
HOP = hop --clear-cache

HOPNAME = hop
HOPMACRONAME = Hop

HOPSH = hopsh
PDFLATEX = TEXINPUTS=.:./latex:$$TEXINPUTS pdflatex
LATEX = latex
DVIPS = dvips -E
BIBTEX = bibtex
WGET = wget
GNUPLOT = gnuplot
JSBENCHDIR = $$HOME/prgm/project/hop/jsbench/tools

SHELL = bash

LIBSDIR = $$TMPDIR/$$USER/libs

#*---------------------------------------------------------------------*/
#*    Target and sources                                               */
#*---------------------------------------------------------------------*/
TARGET = jsdynprop
BIB = jsdynprop.bib

FIGURES = svg/ropes svg/index svg/dynprop

LATEXSRC = latex/acmart.tex hoplst.tex macros.tex

SOURCES = $(TARGET).tex $(LATEXSRC) 0-abstract.tex 1-intro.tex \
  2-strings.tex 3-dynprop.tex 4-proxy.tex 5-perfs.tex \
  6-related.tex 7-conclusion.tex

SRCS = icache.c idyncache.c dyncache.c name.c proxy.js forinarr.js \
  cache-hit.js icache-pmap.c

BENCHS = forinarr.pdf forinobj.pdf sieve.pdf \
  cache.pdf proxy.pdf hopproxy.pdf hopproxy-rel.pdf \
  plain.pdf ratio.pdf noscache.pdf

BENCHJS = bague \
   boyer \
   base64 \
   binary-tree \
   crypto \
   crypto-aes \
   crypto-sha1 \
   crypto-md5 \
   deltablue \
   earley \
   fannkuch \
   hash-map \
   maze \
   puzzle \
   qsort \
   richards \
   sieve \
   splay

BENCHSOMEJS = crypto-aes-proxy-some

EDIR = $$PWD/bench/engines

ALLENGINES = -e $(EDIR)/graal.json \
  -e $(EDIR)/hop.json \
  -e $(EDIR)/jsc.json \
  -e $(EDIR)/js60.json \
  -e $(EDIR)/nodejs.json

HOPENGINES = -e $(EDIR)/hop.json \
  -e $(EDIR)/hopnoscache.json \
  -e $(EDIR)/hopnoopt.json \
  -e $(EDIR)/hopnopcache.json \
  -e $(EDIR)/hopfullcache.json

#*---------------------------------------------------------------------*/
#*    Suffixes                                                         */
#*---------------------------------------------------------------------*/
.SUFFIXES:
.SUFFIXES: .tex .pdf .svg .json .log.json .noscache.log.json

#*---------------------------------------------------------------------*/
#*    All                                                              */
#*---------------------------------------------------------------------*/
.PHONY: all pdf html xhtml re hop.tex

all: pdf ae.pdf

ae.pdf: ae.tex ae-appendix.tex
	$(PDFLATEX) ae.tex


pdf: $(ALLBENCHMARKS)
	$(MAKE) $(TARGET).pdf

html:
	$(MAKE) $(TARGET).html

xhtml:
	$(MAKE) $(TARGET).xhtml

re:
	$(PDFLATEX) $(TARGET).tex
	$(BIBTEX) $(TARGET)

tex: 
	$(MAKE) $(TARGET).tex

$(TARGET).pdf: $(BIB) $(SOURCES) proxy-plain.tex hop.tex \
  $(SRCS:%=src/%) $(FIGURES:%=%.pdf) $(BENCHS) $(BENCHSOMEJS).pdf
	$(PDFLATEX) $(TARGET).tex
	$(BIBTEX) $(TARGET)
	$(PDFLATEX) $(TARGET).tex
	$(PDFLATEX) $(TARGET).tex

hop.tex:
	echo "\\newcommand\\hop{\\textsf{$(HOPMACRONAME)}\\xspace}" > hop.tex

#*---------------------------------------------------------------------*/
#*    Rules                                                            */
#*---------------------------------------------------------------------*/
.SUFFIXES: .data .pdf .svg .svgz .js .log .json .size.json

%.pdf: %.svg
	inkscape -A $@ $<

%.pdf: svg/%.svg
	inkscape -A $@ $<

%.pdf: %.svgz
	inkscape -A $@ $<

#*---------------------------------------------------------------------*/
#*    benchmarks                                                       */
#*---------------------------------------------------------------------*/
forinarr.pdf: bench/LOGS/forinarr.log.json bench/LOGS/forkeysarr.log.json \
  bench/LOGS/forstrarr.log.json bench/LOGS/foridxarr.log.json
	$(HOP) --no-server -- $(JSBENCHDIR)/logbench.js gnuplothistogram.js $^ \
           --format pdf \
	   $(ALLENGINES) \
           -o $@ \
           --xtics="" \
           --ylabel="execution time (in sec)" \
           --title="Performance of string array indexes" \
           --logscale=y	\
           --errorbars="rgb '#444444'" \
           2> forinarr.csv.tmp \
           | sed -e 's/hop/$(HOPNAME)/g' \
                 -e 's/nodejs/v8/g' \
           > forinarr.plot
	cat forinarr.csv.tmp \
           | sed -e 's/foridxarr/foridx/g' \
                 -e 's/forinarr/forin/g' \
                 -e 's/forkeysarr/forkey/g' \
                 -e 's/forstrarr/forstr/g' \
	   > forinarr.csv && rm forinarr.csv.tmp
	$(GNUPLOT) forinarr.plot

forinobj.pdf: bench/LOGS/forinobj-switch.log.json bench/LOGS/forinobj-dyn.log.json
	$(HOP) --no-server -- $(JSBENCHDIR)/logbench.js gnuplothistogram.js $^ \
           --format pdf \
	   $(ALLENGINES) \
           -o $@ \
           --nosort \
           --xtics="" \
           --ylabel="execution time (in sec)" \
           --title="" \
           --errorbars="rgb '#444444'" \
           --logscale=y	\
           2> forinobj.csv.tmp \
           | sed -e 's/hop/$(HOPNAME)/g' \
                 -e 's/nodejs/v8/g' \
           > forinobj.plot
	cat forinobj.csv.tmp \
           | sed -e 's/forinobj-switch/keys-static/g' \
                 -e 's/forinobj-dyn/keys-dynamic/g' \
           > forinobj.csv && rm forinobj.csv.tmp
	$(GNUPLOT) forinobj.plot

sieve.pdf: bench/LOGS/sieve.log.json bench/LOGS/sieve-dyn.log.json
	$(HOP) --no-server -- $(JSBENCHDIR)/logbench.js gnuplothistogram.js $^ \
           --format pdf \
	   $(ALLENGINES) \
           -o $@ \
           --nosort \
           --xtics="" \
           --ylabel="execution time (in sec)" \
           --title="Compared performance of property accesses" \
           2> sieve.csv.tmp \
           | sed -e 's/hop/$(HOPNAME)/g' \
                 -e 's/nodejs/v8/g' \
           > sieve.plot
	cat sieve.csv.tmp \
           | sed -e 's/sieve/sieve-static/g' \
                 -e 's/sieve-static-dyn/sieve-dynamic/g' \
           > sieve.csv && rm sieve.csv.tmp
	$(GNUPLOT) sieve.plot

cache.pdf: bench/LOGS/cache-mono.log.json bench/LOGS/cache-miss.log.json
	$(HOP) --no-server -- $(JSBENCHDIR)/logbench.js gnuplothistogram.js $^ \
           --format pdf \
	   $(ALLENGINES) \
           -o $@ \
           --nosort \
           --xtics="" \
           --ylabel="execution time (in sec)" \
           --title="Performance of cache misses" \
           2> cache.csv.tmp \
           | sed -e 's/hop/$(HOPNAME)/g' \
                 -e 's/nodejs/v8/g' \
           > cache.plot
	cat cache.csv.tmp \
           | sed -e 's/cache-mono/cache-hit/g' \
           > cache.csv && rm cache.csv.tmp
	$(GNUPLOT) cache.plot

ratio.pdf: $(BENCHJS:%=%.ratio.log.json)
	$(HOP) --no-server -- $(JSBENCHDIR)/logbench.js gnuplothistogram.js $^ \
           --format pdf \
	   $(ALLENGINES) \
           -o $@ \
           --lmargin=6 --rmargin=1 \
           --logscale=y --xtics="rotater" \
           --ylabel="proxy/plain" \
           --ylabelopt="offset 2,0" \
           --title="Proxy impact" \
           --font="Verdana,6" \
           --unitRatio=1 \
           --errorbars="rgb '#444444'" \
           2> ratio.csv.tmp \
           | sed -e 's/hop/$(HOPNAME)/g' \
                 -e 's/nodejs/v8/g' \
           > ratio.plot \
           && cat ratio.csv.tmp | sed -e 's/-proxy/+/' > ratio.csv \
           && $(GNUPLOT) ratio.plot \
           && rm ratio.csv.tmp

%.ratio.log.json: bench/LOGS/%-proxy.log.json bench/LOGS/%.log.json
	$(HOP) --no-server -- $(JSBENCHDIR)/logbench.js ratio.js $^ \
        > $@

noscache.pdf: $(BENCHJS:%=%.noscache.log.json)
	$(HOP) --no-server -- $(JSBENCHDIR)/logbench.js gnuplothistogram.js $^ \
           --format pdf \
           -o $@ \
           --rmargin=1 \
           --xtics="rotater" \
           --xticsFont=", 12" \
           --ylabel="regular/extended strings" \
           --ylabelopt="offset 0,0" \
           --yrange="[0.95:1.1]" \
           --title="" \
           --font="Verdana,16" \
           --unitRatio=1 \
           --lineStyle=3 \
           2> noscache.csv \
           | sed -e 's/hop/$(HOPNAME)/g' \
           > noscache.plot && $(GNUPLOT) noscache.plot

%.noscache.log.json: bench/LOGS/%.log.json
	$(HOP) --no-server -- $(JSBENCHDIR)/logbench.js ratioengine.js $^ \
          -e $(EDIR)/hop.json -e $(EDIR)/hopnoscache.json  \
          > $@.tmp.json
	$(HOP) --no-server -- bench/logmem.js $@.tmp.json > $@
	rm $@.tmp.json

#*---------------------------------------------------------------------*/
#*    Proxy benchmarks                                                 */
#*---------------------------------------------------------------------*/
PROXY_LOGS = $(BENCHJS:%=bench/LOGS/%-proxy.log.json)

proxy.pdf: $(PROXY_LOGS)
	$(HOP) --no-server -- $(JSBENCHDIR)/logbench.js gnuplothistogram.js $^ \
           --format pdf \
	   $(ALLENGINES) \
           -o $@ \
           --lmargin=6 --rmargin=1 \
           --xtics="rotater" \
           --ylabel="execution time (in sec)" \
           --ylabelopt="offset 0,0" \
           --logscale=y \
           --title="Proxy performance" \
           --font="Verdana,6" \
           --errorbars="rgb '#444444'" \
           2> proxy.csv.tmp \
           | sed -e 's/hop/$(HOPNAME)/g' \
                 -e 's/nodejs/v8/g' \
           > proxy.plot \
           && cat proxy.csv.tmp | sed -e 's/-proxy/+/' > proxy.csv \
           && $(GNUPLOT) proxy.plot \
           && rm proxy.csv.tmp

hopproxy.pdf: $(PROXY_LOGS)
	$(HOP) --no-server -- $(JSBENCHDIR)/logbench.js gnuplothistogram.js $^ \
           --format pdf \
	   $(HOPENGINES) \
           -o $@ \
           --lmargin=6 --rmargin=1 \
           --xtics="rotater" \
           --ylabel="execution time (in sec)" \
           --ylabelopt="offset 0,0" \
           --logscale="y 2" \
           --lineStyle=3 \
           --title="Proxy performance" \
           --font="Verdana,8" \
           --errorbars="rgb '#444444'" \
           2> hopproxy.csv.tmp \
           | sed -e 's/hop/$(HOPNAME)/g' \
                 -e 's|hopnoopt|hop w/o contrib. #3abd|g' \
                 -e 's|hopnopcache|hop w/o contrib. #3c|g' \
                 -e 's|hopfullcache|hop w/o contrib. #3d|g' \
                 -e 's|hopnoscache|hop w/o contrib. #2|g' \
           > hopproxy.plot \
           && cat hopproxy.csv.tmp | sed -e 's/-proxy/+/' > hopproxy.csv \
           && $(GNUPLOT) hopproxy.plot \
           && rm hopproxy.csv.tmp

hopproxy-rel.pdf: $(PROXY_LOGS)
	$(HOP) --no-server -- $(JSBENCHDIR)/logbench.js gnuplothistogramrel.js $^ \
           --format pdf \
	   $(HOPENGINES) \
           -o $@ \
           --lmargin=8 --rmargin=1 \
           --xtics="rotater" \
           --ylabel="compared execution times" \
           --ylabelopt="offset 0,0" \
           --logscale="y 2" \
           --lineStyle=3 \
           --title="Proxy performance" \
           --font="Verdana,8" \
           2> hopproxy-rel.csv.tmp \
           | sed -e 's/hop/$(HOPNAME)/g' \
                 -e 's|hopnoopt|hop w/o contrib. #3abd|g' \
                 -e 's|hopnopcache|hop w/o contrib. #3c|g' \
                 -e 's|hopfullcache|hop w/o contrib. #3d|g' \
                 -e 's|hopnoscache|hop w/o contrib. #2|g' \
           > hopproxy-rel.plot \
           && cat hopproxy-rel.csv.tmp | sed -e 's/-proxy/+/' > hopproxy-rel.csv \
           && $(GNUPLOT) hopproxy-rel.plot \
           && rm hopproxy-rel.csv.tmp

$(BENCHSOMEJS).pdf: $(BENCHSOMEJS).plot $(BENCHSOMEJS).csv
	$(GNUPLOT) $(BENCHSOMEJS).plot

$(BENCHSOMEJS).plot:
	cat bench/RANGE/$(BENCHSOMEJS).plot \
          | sed -e 's/90/0/g' \
                -e 's/-proxy-some.js/+/' \
                -e 's/left box/right box/' \
          > $(BENCHSOMEJS).plot

$(BENCHSOMEJS).csv:
	cat bench/RANGE/$(BENCHSOMEJS).csv \
          | sed -e 's/^1 /100% /' \
                -e 's/^2 /50% /' \
                -e 's/^3 /33% /' \
                -e 's/^4 /25% /' \
                -e 's/^5 /20% /' \
                -e 's/^6 /16% /' \
                -e 's/^7 /14% /' \
                -e 's/^8 /12% /' \
                -e 's/^9 /11% /' \
                -e 's/^10 /10% /' \
          > $(BENCHSOMEJS).csv

#*---------------------------------------------------------------------*/
#*    Plain benchmarks                                                 */
#*---------------------------------------------------------------------*/
PLAIN_LOGS = $(PROXY_LOGS:%-proxy.log.json=%.log.json)

plain.pdf: $(PLAIN_LOGS)
	$(HOP) --no-server -- $(JSBENCHDIR)/logbench.js gnuplothistogram.js $^ \
           --format pdf \
	   $(ALLENGINES) \
           -o $@ \
           --lmargin=6 --rmargin=1 \
           --logscale=y --xtics="rotater" \
           --ylabel="execution time (in sec)" \
           --ylabelopt="offset 5,0" \
           --title="Plain performance" \
           --font="Verdana,8" \
           2> plain.csv \
           | sed -e 's/hop/$(HOPNAME)/g' \
                 -e 's/nodejs/v8/g' \
           > plain.plot && $(GNUPLOT) plain.plot

#*---------------------------------------------------------------------*/
#*    Proxy + Plain                                                    */
#*---------------------------------------------------------------------*/
proxy-plain.tex: $(PLAIN_LOGS) $(PROXY_LOGS)
	$(HOP) -g --no-server -- $(JSBENCHDIR)/logbench.js latex $^ \
            --sortalias=crypto-proxy:crypto-aaa \
	    $(ALLENGINES) \
            | sed "s/3em/5.8em/g" \
            | sed "s/\\\\textit{benchmark}/\\\\makebox[8.5em][l]{\\\\textit{benchmark}}/g" \
            | sed "s/nodejs/v8/g" \
            | sed "s/hop}/\\\\hop}/" \
            | sed "s/\\\\texttt{base64}/\\\\hdashline\\\\texttt{base64}/" \
            | sed "s/\\\\texttt{binary-tree}/\\\\hdashline\\\\texttt{binary-tree}/" \
            | sed "s/\\\\texttt{crypto-aes}/\\\\hdashline\\\\texttt{crypto-aes}/" \
            | sed "s/\\\\texttt{crypto-md5}/\\\\hdashline\\\\texttt{crypto-md5}/" \
            | sed "s/\\\\texttt{crypto-sha1}/\\\\hdashline\\\\texttt{crypto-sha1}/" \
            | sed "s/\\\\texttt{deltablue}/\\\\hdashline\\\\texttt{deltablue}/" \
            | sed "s/\\\\texttt{earley}/\\\\hdashline\\\\texttt{earley}/" \
            | sed "s/\\\\texttt{fannkuch}/\\\\hdashline\\\\texttt{fannkuch}/" \
            | sed "s/\\\\texttt{hash-map}/\\\\hdashline\\\\texttt{hash-map}/" \
            | sed "s/\\\\texttt{maze}/\\\\hdashline\\\\texttt{maze}/" \
            | sed "s/\\\\texttt{puzzle}/\\\\hdashline\\\\texttt{puzzle}/" \
            | sed "s/\\\\texttt{qsort}/\\\\hdashline\\\\texttt{qsort}/" \
            | sed "s/\\\\texttt{richards}/\\\\hdashline\\\\texttt{richards}/" \
            | sed "s/\\\\texttt{crypto}/\\\\hdashline\\\\texttt{crypto}/" \
            | sed "s/\\\\texttt{boyer}/\\\\hdashline\\\\texttt{boyer}/" \
            | sed "s/\\\\texttt{sieve}/\\\\hdashline\\\\texttt{sieve}/" \
            | sed "s/\\\\texttt{splay}/\\\\hdashline\\\\texttt{splay}/" \
            | sed "s/bague}/bague}$$^{\\\\star}$$/" \
            | sed "s/base64}/base64}$$^{\\\\circ}$$/" \
            | sed "s/boyer}/boye}r$$^{\\\\dagger}$$/" \
            | sed "s/crypto}/crypto}$$^{\\\\dagger}$$/" \
            | sed "s/crypto-aes}/crypto-aes}$$^{\\\\ast}$$/" \
            | sed "s/crypto-md5}/crypto-md5}$$^{\\\\ast}$$/" \
            | sed "s/crypto-sha1}/crypto-sha1}$$^{\\\\ast}$$/" \
            | sed "s/hash-map}/hash-map}$$^{\\\\ast}$$/" \
            | sed "s/earley}/earley}$$^{\\\\dagger}$$/" \
            | sed "s/fannkuch}/fannkuch}$$^{\\\\diamond}$$/" \
            | sed "s/binary-tree}/binary-tree}$$^{\\\\diamond}$$/" \
            | sed "s/fib}/fib}$$^{\\\\star}$$/" \
            | sed "s/js-beautify}/js-beautify}$$^{\\\\diamond}$$/" \
            | sed "s/maze}/maze}$$^{\\\\star}$$/" \
            | sed "s/puzzle}/puzzle}$$^{\\\\star}$$/" \
            | sed "s/qsort}/qsort}$$^{\\\\star}$$/" \
            | sed "s/richards}/richards}$$^{\\\\dagger}$$/" \
            | sed "s/deltablue}/deltablue}$$^{\\\\dagger}$$/" \
            | sed "s/sieve}/sieve}$$^{\\\\star}$$/" \
            | sed "s/splay}/splay}$$^{\\\\dagger}$$/" \
            | sed "s/tagcloud}/tagcloud}$$^{\\\\ast}$$/" \
            | sed "s/acorn}/acorn}$$^{\\\\bullet}$$/" \
            | sed "s/babylon}/babylon}$$^{\\\\bullet}$$/" \
            | sed "s|hdashline|hdashline[2pt/1pt]|" \
            > $@

#*---------------------------------------------------------------------*/
#*    Clean                                                            */
#*---------------------------------------------------------------------*/
.PHONY: clean cleanall

clean:
	/bin/rm -f hop.tex
	/bin/rm -f $(TARGET).{log,aux,bbl,blg,out} 
	/bin/rm -f jsprop-serrano.UTF-8.bib jsprop.UTF-8.bib
	/bin/rm -f $(BENCHS) $(BENCHS:%.svg=%.pdf)
	/bin/rm -f ae.aux ae.log ae.out
	/bin/rm -f $(BENCHSOMEJS).pdf

cleanall: clean
	/bin/rm -f $(TARGET).pdf $(TARGET).html $(TARGET).xhtml
	/bin/rm -f jsdynprop-serrano.zip
	/bin/rm -f jsdynprop.aux jsdynprop.blg jsdynprop.log jsdynprop.bbl
	/bin/rm -f src/a.out src/*.o
	/bin/rm -f $(BENCHS:%.svg=%.plot) $(BENCHS:%.svg=%.csv)
	/bin/rm -f $(BENCHSOMEJS).plot $(BENCHSOMEJS).csv
	/bin/rm -f *.ratio.log.json
	/bin/rm -f *.noscache.log.json
	/bin/rm -f ae.pdf

export:
	(cd .. && zip -r jsdynprop-$(shell date +"%d%b%Y-%Hh%M").zip \
		 jsdynprop/ -x 'jsdynprop/CC19' 'jsdynprop/webtooling' 'jsdynprop/bench.log' 'jsdynprop/engines' 'jsdynprop/node_modules' 'jsdynprop/realworld' 'jsdynprop/tools' 'jsdynprop/.git/*' 'jsdynprop/*.wiki' 'jsdynprop/svg/*.svg' jsdynprop/Makefile 'jsdynprop/etc/*' 'jsdynprop/*.log' 'jsdynprop/*.bbl' 'jsdynprop/*.blg' 'jsdynprop/*.css' 'jsdynprop/*.hop' 'jsdynprop/*.hss' 'jsdynprop/jsdynprop.pdf' 'jsdynprop/jsdynprop.aux' 'jsdynprop/*.svg*' 'jsdynprop/docker/*' 'jsdynprop/bench/LOGS/*' 'jsdynprop/bench/*.patch' 'jsdynprop/logmem.js' jsdynprop/ae.pdf jsdynprop/AE.sh jsdynprop/AE-INSTALL.sh jsdynprop/README.md 'jsdynprop/proxy-plot-alt.*')

export-ae:
	(cd .. && zip -r jsdynprop-ae-$(shell date +"%d%b%Y-%Hh%M").zip \
             jsdynprop/AE.sh jsdynprop/AE-INSTALL.sh jsdynprop/ae.pdf jsdynprop/README.md jsdynprop/docker jsdynprop/README.txt jsdynprop/LICENSE.txt)
