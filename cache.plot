set terminal pdf font "Verdana,18"
set output 'cache.pdf'

set title 'Performance of cache misses'
set ylabel "execution time (in sec)" 

set auto x

set style data histogram
set style histogram cluster gap 1
set xtics 
set boxwidth 0.9
set style fill solid
set style line 1 linecolor rgb '#3264c8' linetype 1 linewidth 1
set style line 2 linecolor rgb '#d83812' linetype 1 linewidth 1
set style line 3 linecolor rgb '#fa9600' linetype 1 linewidth 1
set style line 4 linecolor rgb '#109318' linetype 1 linewidth 1
set style line 5 linecolor rgb '#960096' linetype 1 linewidth 1

set grid ytics
set xtics scale 0
set datafile separator ","

set key under nobox

plot \
  'cache.csv' u 2:xtic(1) title 'graal' ls 1, \
  'cache.csv' u 3:xtic(1) title 'hop' ls 2, \
  'cache.csv' u 4:xtic(1) title 'jsc' ls 3, \
  'cache.csv' u 5:xtic(1) title 'js60' ls 4, \
  'cache.csv' u 6:xtic(1) title 'v8' ls 5

