%*=====================================================================*/
%*    serrano/diffusion/article/jsdynprop/4-proxy.tex                  */
%*    -------------------------------------------------------------    */
%*    Author      :  Manuel Serrano                                    */
%*    Creation    :  Wed Aug  7 12:19:34 2019                          */
%*    Last change :  Thu Jan  9 17:11:43 2020 (serrano)                */
%*    Copyright   :  2019-20 Manuel Serrano                            */
%*    -------------------------------------------------------------    */
%*    Proxies                                                          */
%*=====================================================================*/

%*---------------------------------------------------------------------*/
%*    Proxies                                                          */
%*---------------------------------------------------------------------*/
\section{Proxy Objects}
\label{proxy}

To quote Mozilla's MDN~\cite{mdn:proxy}, a proxy object is used to
define custom behavior for fundamental operations (\emph{e.g.,}
property lookup, assignment, enumeration, function invocation,
etc). It is constructed with: \TT{new Proxy(target, handler)}. The
argument \TT{target} is the original object and \TT{handler} is the
container for the functions, \emph{a.k.a,} \emph{traps}, to be invoked
when primitive operations are to be executed on \TT{target}. Here is
MDN web site proxy example.

\lstinputlisting[numbers=left, xleftmargin=0.38cm, language=hop]{src/proxy.js}

Proxy are expected to slow down execution for four reasons:

\begin{enumerate}
 \itemsep0.3em
 \item They replace property accesses that are optimized by inline caches
   with more expensive function calls.
 \item They require allocating at least twice as many objects (the original
   object, the proxy, and, sometimes, the handler), so they exercise the
   memory allocator and the garbage collector.
 \item In the client implementation of traps, target object
   properties are generally accessed with dynamic property names,
   as the property name is an argument to the trap. The performance
   consequences of dynamic property names are severe because the conventional
   inline cache optimization does not apply.
 \item Because both the target \emph{and} the handler can evolve
   independently from one another, the inline caching
   should be able to accommodate two independent chan\-ges between two accesses
   to a proxy. Unfortunately, this is not possible with the currently known techniques,
   the inline caching optimization is effectively defeated for all proxied objects.
\end{enumerate}

\noindent Problems~\#1 and \#2 are intrinsic to the very nature of
proxy objects. They are unlikely to be eliminated. Problem~\#3 is
mitigated by the technique presented Section~\ref{dyn-prop-access}.
In the following section we present the solution we propose for
improving problem~\#4.

\subsection{General Implementation}

Read and write property accesses are the most important operations for
proxy performance. Their implementations follow the same principles so
we present only one here, namely the write operation. Neglecting error
cases for simplicity, the \js standard semantics specify the following
operations:

\begin{enumerate}
 \itemsep0.3em
 \item check if the proxy object has been revoked.
 \item get the handler's \TT{set} property.
 \item if \TT{set} is a function, then:
 \begin{enumerate}
  \itemsep0.1em
  \item check if the value is compatible with the target.
  \item if it is, invoke the \TT{set} function with three arguments:
   the target, the property, the value, and handler is the receiver
   of the method.
  \end{enumerate}
 \item if \TT{set} is a proxy and if that proxy has an \TT{apply} trap,
  apply it as step in 3.
 \item otherwise, assign the property to target.
\end{enumerate}

\noindent \contribution{3} We propose techniques for optimizing proxy
read and write property accesses. They are evaluated in
Section~\ref{perf:proxy}.

The number of steps and their complexity deter inlining the proxy
write sequence inside client code, but we take advantage of the
general cache miss sequence to recover the performance.  In
particular, the proxy sequence is inlined in the \TT{cacheWriteMiss}
function.

Previous studies of polymorphic inline
caches~\cite{Holzle:1991,sf:cc19} show that they eliminate almost all cache
misses for non-proxy programs. But, when a property is
read from a proxy object, it always ends up with hidden class
comparison failure, as proxy objects do not have properties themselves.
This observation justifies that cache misses should favor proxy objects
over regular objects (\contri{3a}).

\iflongversion
It should be possible to use the target's hidden class, instead of
proxy's one, when filling the cache after a miss with a proxy, but then,
all cache hits would require an extra test in the client code to
distinguish between situations where the target is used directly from
situations where the target is used via the proxy. The consequence
would be to add an extra test for all accesses, even those that do not
use proxy at all. As a fraction of all accesses, proxies accesses are rare so
we do not use this solution in \hop.
\fi

Let us now detail how each semantics step
is implemented:

\noindent \emph{Step 1}: \hop compiles \js files into an extension of
the Scheme language~\cite{scheme:r5rs}. \js objects are implemented
as Scheme classes, each primitive \js type being mapped to a dedicated
class.  \hop's Scheme implementation allows instances to
store extra information along with the class descriptor at no
cost. This is already used by \hop to store information about each
object. For instance, one bit is used for denoting inlined arrays,
another bit is used for denoting objects that have only regular
properties, \emph{i.e.,} read/write/configurable properties, another
bit is used to mark sealed object, etc. We used that possibility in
our extension for encoding revoked proxies.  Testing proxy revocation
is then a simple bit comparison (\contri{3b}).

%*---------------------------------------------------------------------*/
%*    Proxy performance                                                */
%*---------------------------------------------------------------------*/
\begin{figure*}[htb]
\centerline{\includegraphics[width=0.9\linewidth]{./proxy.pdf}}
\caption{Comparison of proxy performance. Bars represent the mean of 30
  execution times for each tested system and for each test. The bars for each implementation
  are in the same order as in the legend. Smaller is better; log scale.
\label{perf:proxy.fig}}
\end{figure*}



\noindent \emph{Step 2}: Getting the \TT{set} attribute of the proxy
handler is a normal property access. It can then be optimized using
classical inline caches. The critical issue is where and when to
allocate the associated cache? The conventional approach, where one
cache is allocated per syntactic occurrences of the reference to the
\TT{set} attribute, does not work, as every access to any proxy
object's \TT{set} attribute has the same source location (as mentioned
above, the proxy access sequence is not inlined), namely the part of
the runtime system that implements proxies.

Our solution is to have proxies carrying their own caches
(\contri{3c}). This, however, has its own danger, as it increases the
size of proxy objects. Thus, in order to avoid excessive memory
allocation, we allocate shared caches, one cache per proxy allocation
site in the original program (\contri{3d}). That is, all proxies
allocated from the same location will share the same cache for their
\TT{set} and \TT{get} retrieval. This is generally a good strategy as
all proxies allocated on the same site are likely to share their
handlers.

\noindent \emph{Step 3a}: When assigning a value to a proxy object,
\js imposes various constraints depending on the definition of the
property. For instance, if the property is read-only, the assigned
value must be the same as the already stored one. Implementing these
checks, in general, require us to retrieve and inspect the property
descriptor object of the target~\footnote{Each \js object must
implement the {\TT{get\-Own\-Property\-Descriptor}} method that
returns per-object description of the object's properties and
information about them. Allocating it ahead of time is a significant
space cost and implementations generally create it lazily.}.
Most objects, however, have (morally) the same property descriptor,
namely one that says that all of the properties are
read/write. Accordingly, \hop simply has a single bit recorded with
the object's runtime representation indicating it has this kind of
descriptor. The proxy object runtime support must, therefore, be able
to access this bit and also short-circuit the creation of the property
descriptor, so the proxy check can be replaced with a check that is a
mere arithmetic comparison (\contri{3e}).

All the other steps are compiled using standard \hop methods and
optimizations.

%%%
\ifcachemisses
\subsection{Caching Cache Misses}
\label{caching-cache-misses}

{\small{\fbox{\footnotesize{\textbf{\ding{42} Note:}}}}} This section
is of interest only for AOT compilers. Readers only interested in
techniques applicable to JIT compilers too can safely skip it.

Proxy objects can be used to intercept much more than get and set
operations. They can intercept function calls, object creation,
prototype changes, property ownership tests, etc. However, an handler
is not required to implement all the possible traps. For instance,
in the example of Figure~\ref{mdn-proxy-example}, the handler only
takes over the \emph{get} operation but does not intercept \emph{set}
or \emph{has} operations. When an non-intercepted operation is applied
to a proxy, that operation must be executed on the target object as if
no proxy was interposed. For instance, if a field is added to the
proxy of the example Figure~\ref{mdn-proxy-example}, as the proxy's
handler does not define a \emph{set} trap, that field will be indeed
added to target object itself. To implement that behavior, the builtin
proxy support tries to fetch the handler's \TT{set} property. If found, it
executes it, passing the target object, the property name, and the new
value. Otherwise, it adds the property directly to the object. In such a
case, the operation \TT{handler["set"]} will always miss the inline cache,
as \TT{handler} does not have such an attribute.

Trying to get a property that an object does not have can be a
terribly slow operation. First, the test against the hidden class is
wasted because the test always fails.  Second, an extra function call
is imposed for invoking the cache miss routine. Third, in that
routine, the missing property name will be compared to all the object
property names and to all its prototype objects, up to the root
prototype object. All the property names of the prototype chain will
be compared to the looked property name for nothing.

\noindent \contribution{4} To prevent this useless but dramatically
slow code execution, the solution is to cache the cache misses. JIT
compilers can circumvent the need for a cache miss cache by simply
generating code that produces \TT{undefined} after a successful hidden
class comparison. This technique is not applicable to ahead-of-time
compilers~\cite{Chandra:2016:TIS:2983990.2984017,v8jitless,serrano:dls18},
but fortunately they can adapt the accessor property compilation
proposed by~\citet{sf:cc19}~\footnote{That work presents the
invalidation rules for the technique that we do not repeat here.}. It
consists of extending the inline cache with an extra tests when the
direct hidden class comparison fails:

\lstinputlisting[numbers=left, xleftmargin=0.38cm, language=C]{src/icache-pmap.c}

The property cache is extended with two new attributes: the object
owning the accessor property (\TT{cache.owner}) and accessor class
(\TT{cache.pclass}). These two attributes are filled in the cache miss
routine. When the property is not found in the object and its
prototype chain, the \TT{cache.owner} attribute is set to a private
pre-allocated object that contains exactly one accessor field whose
value is \TT{undefined}. Section~\ref{perf:cache-misses} evaluates the
performance of this technique. It shows that the performance is not as
good as the JIT technique because of the overhead of the double memory
read, but it still significantly improves on the cost of executing the
normal lookup procedure.
\fi
%%%
