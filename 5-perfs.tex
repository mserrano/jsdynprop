%*=====================================================================*/
%*    serrano/diffusion/article/jsdynprop/5-perfs.tex                  */
%*    -------------------------------------------------------------    */
%*    Author      :  Manuel Serrano                                    */
%*    Creation    :  Fri Aug  9 09:13:21 2019                          */
%*    Last change :  Sun Jan 12 06:53:17 2020 (serrano)                */
%*    Copyright   :  2019-20 Manuel Serrano                            */
%*    -------------------------------------------------------------    */
%*    Performance evaluation                                           */
%*=====================================================================*/

%*---------------------------------------------------------------------*/
%*    Proxy performance                                                */
%*---------------------------------------------------------------------*/
%* \begin{figure*}[htb]                                                */
%* \centerline{\includegraphics[width=1\linewidth]{./proxy.pdf}}       */
%* \caption{Comparison of proxy performance. Bars represent the mean of 30 */
%*   execution times for each tested system and for each test. The bars for each implementation */
%*   are in the same order as in the legend. Smaller is better; linear scale. */
%* \label{perf:proxy.fig}}                                             */
%* \end{figure*}                                                       */

\begin{figure*}[htb]
\centerline{\includegraphics[width=0.9\linewidth]{./ratio.pdf}}
\caption{Impact of using proxies on performance. Each bar represents the ratio
between the original test and the proxied version. Smaller is better; log scale.
\label{perf:ratio.fig}}
\end{figure*}

%*---------------------------------------------------------------------*/
%*    Experimental Evaluation                                          */
%*---------------------------------------------------------------------*/
\section{Experimental Evaluation}
\label{perfs}

A 64-bit Intel Xeon E5-1650 running Linux 4.19/Debian was used for
our performance evaluation. Each test is executed 30 times and the median
wall clock time with relative standard deviation is collected.

We measure Google's \VH \VHv, Java\-ScriptCore \JSCv (\JSC),
SpiderMonkey \JSMv (\JSM), Oracle's Graal \GRAv (\GRA), and a modified
version of \hop 3.3.0. As all these systems (except \hop) use JIT
compilers, we tuned the time of each run to be sufficiently long so
that the warm-up time of the JIT is negligible.

We have used different tests depending on the experiment. For testing
specific features in isolation, we developed dedicated micro
benchmarks. For testing global performance, we use the benchmark suite
used for evaluating \hop general performance~\cite{sf:cc19}. This test
suite excludes some of the classical \js benchmark tests because \hop
does not optimize the standard library functions as well as \VH, \JSC,
and \JSM do. For instance, \hop uses a slow regular engine (pcre
based). As a consequence, all the benchmarks that use regexps
extensively are biased by that slow library implementation and we have
excluded them from hereto presentation. There are similar issues for
floating point numbers. \hop uses the Boehm's collector~\cite{bw:spe88},
a non-copying collector that allocates and deallocates slowly. Using
floating point intensive benchmarks would measure these mismatches,
and not the proxy access.

%*---------------------------------------------------------------------*/
%*    Proxy                                                            */
%*---------------------------------------------------------------------*/
\subsection{Performance of Proxy Objects}
\label{perf:proxy}

The main objective of the optimizations presented in this paper is to
improve proxies performance so that they can be used in more
situations. In this section we compare their performance with \hop and
with the other \js implementations.

Proxies are relatively new to \js and no standard performance test is
available, so we created our own for the experiment. We reused
mid-size programs coming from different classical \js benchmark suites
(Octane, Sunspider, Jetstream, Shootout, and some other tests used in
previous \js evaluation reports) that we modified so that each object
or array creation is replaced with an equivalent proxy creation that
simply executes the corresponding action to its target object.

\iflongversion
Allocation sites \texttt{new CTOR(arg0, arg1, ...)} are replaced
with:
\begin{lstlisting}[language=hop]
const handler = {
  get: (target, prop) => target[prop],
  set: (target, prop, val) => target[prop] = val;
}
new Proxy(new CTOR(arg0, arg1, ...), handler);
\end{lstlisting}
\noindent 
\fi
These modifications applied to the test correspond to the
worse-case situation, as all objects and arrays are trapped by
proxies. This over-emphasizes the impact of proxy objects, as needed
for this experiment, and it correspond to a upper bound for slowdown
factors due to proxy accesses in real-life programs.

We have calibrated each test so that the slowest
system executes within 100 seconds and the \hop execution is as close
as possible to 10 seconds.  The results are given in
Figure~\ref{perf:proxy.fig}. They show that \hop outperforms all
systems for all tests except \textsf{binary-tree+} where only
\GRA is able to get slightly better performance and on
\textsf{sieve+} where \VH is able to perform about 40\%
faster. For all other tests the performance difference between \hop
and other implementations is significant. For instance, on the traditional 
\textsf{deltablue+} and \textsf{crypto-md5+} tests, \hop performs about
5$\times$ times faster than all other systems. Notice that \GRA is not
able to run the \textsf{maze} and \textsf{sieve} tests;
in both cases, a stack overflow happens and \GRA exits.

To give a more precise comparison of all proxy implementations, we
also measure proxy impact on general
performance. Figure~\ref{perf:ratio.fig} gives this
\iflongversion
information
~\footnote{The actual execution times are also given in the
Appendix in Figure~\ref{proxy-plain}.}.
\elif
information.
\fi
It shows the execution times
of proxied tests divided by the execution times of the original
corresponding tests. Notice that this figure uses a logarithmic
scale. This test shows that the performance gap between \hop and other
systems is even more important that one may deduce from
Figure~\ref{perf:proxy.fig}. On most tests, \hop performance is
generally in between 2$\times$ to 4$\times$ slower than systems like
\VH~\cite{serrano:dls18,sf:cc19}, but its proxy implementation
bridges that gap and even enables it to outperform other systems. For
instance, on the Octane \textsf{earley} test, the impact of
introducing proxy objects is a slowdown of 1.66 for \hop, but 11.65
for \VH, 29.39 for \JSM, and 36.57 for \JSC. Only \GRA is able to show
comparable slowdown but notice that this system overall is generally
significantly slower.

Figure~\ref{perf:ratio.fig} also gives a general picture of the
performance \hop can deliver for proxy objects. For 11 out of the 16
tests, the proxied programs are within a 10x range of their
corresponding original ones. All programs that show pathologically bad
performance use arrays extensively and the slowdown seems inevitable
as it mostly comes from turning the fast memory reads and writes of
the original version into function calls in the proxy versions. Some
programs, such as \textsf{qsort}, also suffer from an extra problem. They
use large arrays whose indexes overflow the limit from which index
properties are stored in the table (as discussed in
Section~\ref{array-prop-names}), which causes extra memory
allocations.

We have conducted another experiment that evaluates the impact of proxy
objects when not all allocation sites have proxy wrappers. For each test, we measured the
execution times when the percentage of proxied objects varies from
100\% to 10\%. Figure~\ref{perf:hopproxy-some.fig} shows the result of
that experiment for \textsf{crypto-aes+}. The performance of all
systems increases when the percentage of proxies decreases. This
confirms the results of Figure~\ref{perf:ratio.fig}. We have observed
similar results for all tests but for \textsf{base64+}, \textsf{fannkuch+},
\textsf{hash-map+}, and \textsf{puzzle+}. For these four tests, the
performance of \hop executions behaves as it does for \textsf{crypto-aes+}
but \JSC, \JSM, and \VH have more chaotic executions reflected by
discontinuous time curves. We are unsure what causes those irregularities,
as we do not understand those systems' runtime support for proxies well enough.

\begin{figure}[htb]
\centerline{\includegraphics[width=0.9\linewidth]{./crypto-aes-proxy-some.pdf}}
\caption{Execution times depending on the percentage of proxied objects. Smaller is better; linear scale.
\label{perf:hopproxy-some.fig}}
\end{figure}

Figure~\ref{perf:hopproxy.fig} shows the impact of the proxy
optimizations deployed in \hop. It presents the performance of the
optimizations used separately. Obviously, the two most influential
optimizations are the dynamic property names (\textbf{contrib. \#2})
and the proxy internal inline caches
(\textbf{contrib. \#3c}). Disabling internal inline caches always
slows down execution, by a significant factor (up to almost two with
our tests). We then conclude that this optimization should always be
applied.

\begin{figure*}[htb]
\centerline{\includegraphics[width=0.8\linewidth]{./hopproxy-rel.pdf}}
\caption{Impact of each proxy optimization compared to the baseline
compiler (all optimizations switched on). Smaller is better; log scale.
\label{perf:hopproxy.fig}}
\end{figure*}

The dynamic property names optimization has a different impact. It it
slightly slows down some tests and it accelerates significantly
others.  We have measured the cache misses ratio of all these tests
and we have observed that all the tests that suffer slowdowns are
array intensive.  \hop does not use inline caches for accessing arrays
with integer indexes because it relies as much as possible on faster
indexed memory accesses. As a consequence, for array accesses the
test, \texttt{obj->hclass == name->readcache->hclass} (see
Section~\ref{dyn-prop-access}) always fails. The performance penalty
observed in Figure~\ref{perf:hopproxy.fig} comes exclusively from this
test. For all other tests, that is for all tests that spend a
significant portion time on object access, the dynamic property name is
the most beneficial optimization, even for tests, such as
\textsf{richards+}, for which some name overloading causes
dynamic inline cache misses. This experiment also shows that except for one
benchmark (\textsf{crypto+}), allocating caches per allocation-site instead
of per-allocated proxy (contrib. \#3d) is highly beneficial.

%*---------------------------------------------------------------------*/
%*    Impact of string caches                                          */
%*---------------------------------------------------------------------*/
\subsection{Impact of String Caches}
\label{perf:string}

The techniques presented in this paper have very little impact on
the rest of the implementation of the runtime system. The only
modification that hurts performance is the need for extra fields in
the string value implementation. To measure that impact,
we compare the performance
of two versions of \hop that differ only by their representation of
strings and the dynamic property optimization described in
Section~\ref{dynprop}. We have selected tests that do not use dynamic
property names so the lack of dynamic optimization imposes no
penalty. The result are presented in Figure~\ref{perf:noscache.fig}.

\begin{figure}[htb]
\centerline{\includegraphics[width=0.9\linewidth]{./noscache.pdf}}

\caption{Evaluation of the cost of extra information in strings on
non-proxied programs. For each tests, the "time" bar shows the execution
time with normal strings divided by the time of execution time with
extended strings. The "mem" bar shows the memory consumption
comparison. The closer to 1, the smaller the impact.
\label{perf:noscache.fig}}

\end{figure}

The differences between execution times is in the range $\pm$3\% and
even close to 0\% for many programs. We have observed only three
tests for which it yields to a memory footprint increase. As only
strings used as property names need to be extended with inline caches,
it might be that a clever string encoding could eliminate the
allocation of that memory slot for non-name strings, similarly to what
we did when merging the right part of ropes and the write inline
caches.

%*---------------------------------------------------------------------*/
%*    Dynamic Property Names                                           */
%*---------------------------------------------------------------------*/
\subsection{Performance of Dynamic Property Names}
\label{perf:dynamic-property-name}

In order to improve our understanding of the good \hop proxy
performance we designed various micro-benchmarks that attempt to isolate the
speed of the dynamic property name access used by proxy objects.

\begin{figure}[htb]
\centerline{\includegraphics[width=0.7\linewidth]{./forinobj.pdf}}
\caption{Comparing the raw performance of \js implementations for
static and dynamic property names. Smaller is better; log scale.
\label{perf:dynamic-property-name-a.fig}}
\end{figure}

The first test consists of a simple loop that accesses all of the
properties an object holds. The access to object properties is
implemented using two variants. The dynamic version is implemented as
follows:

\lstinputlisting[numbers=none, language=hop, linerange={6-6}]{bench/micro/forinobj.js}

\noindent In the static version the expression \TT{a[keys[i]]} is replaced
with a \TT{switch} that branches according to all possible names used in the
program. The results are presented in
Figure~\ref{perf:dynamic-property-name-a.fig}.

%%% \if
\ifsievedynprop 
\begin{figure}[htb]
\centerline{\includegraphics[width=0.8\linewidth]{./sieve.pdf}}
\caption{Comparing the raw performance of \js implementations for
static and dynamic property names. Smaller is better; log scale.
\label{perf:dynamic-property-name-b.fig}}
\end{figure}
\fi
%%% \fi

\noindent With the exception of \hop, all the implementations impose a
significant cost for the dynamic version. The good performance for
both \hop versions establishes the benefit of the dynamic property
accesses optimization presented in Section~\ref{dyn-prop-access}.

%%% \if
\ifsievedynprop 
For a different perspective, we measured dynamic property accesses
with another test. We modified the \textsf{sieve} benchmark. In the
original version of that test, all the allocated objects have the same
structure, namely two properties.  We took benefit of that regularity
to create a modified version where all the static property accesses
are replaced with dynamic property accesses.  Each access is replaced
with an access where the name is extracted from an array of names. We
compared the execution times of the two versions for each system (see
Figure~\ref{perf:dynamic-property-name-b.fig}).  Note that the dynamic
version is expected to run slower as it executes twice as many memory
reads as the static version. Graal cannot complete this test so no
value is presented for that system. Although \hop does not perform
very well on that test, it shows the smallest slowdown imposed by
introducing dynamic property names (1.3$\times$ for \hop, 2.5$\times$
for \JSM, 2.8$\times$ for \VH, and 5.32$\times$ for \JSC), which also
demonstrates the benefit of our approach.
\fi
%%% \fi

\begin{figure}[htb]
\centerline{\includegraphics[width=0.9\linewidth]{./forinarr.pdf}}
\caption{Comparing the raw performance of \js implementations for array
string array accesses. Smaller is better; log scale.
\label{perf:array-property-name.fig}}
\end{figure}

Next, we measured the performance of index property names.  We know,
from examining the source code, that \VH and \JSM (like \hop)
both use string representations that hold references
to the corresponding index when the string's content is a number.
That said, we still see a performance gap between them and Hop. These
experiments report on the gap, but we do not have an explanation for
the gap based on the representations that V8 and SpiderMonkey are
using. While we cannot claim to fully understand the complete details
of other engines' representations, the picture that these performance
results paint suggests that \hop's representations are the right
choices.

The test \textsf{foridx} measures the performance of array indexes
used as integers. It repeats the loop \TT{r=0; for(let i in a)
r+=(+i)} where \TT{a} is an array and \TT{r} an integer. The test
\textsf{forin} measures array performance of string array indexes with
the loop \TT{for(let i in a) r+=a[i]}. The test \textsf{forkey} is
similar to \textsf{forin} but array indexes are first stored in a
separated array and the loop is \TT{for(let i=0; i<l; i++)
r+=a[k[i]]}.  The test \textsf{forstr} is similar to \textsf{foridx},
but indexes are concatenated as strings. The loop is \TT{s=""; for(let
i in a) s+=i}. Figure~\ref{perf:array-property-name.fig} shows the
scores for these tests. It reveals that systems come with some trade
offs. \JSC is fast for accessing arrays in the loop but all the
regular string operations on indexes are slow.  \VH is reasonably fast
for using array indexes as integers but relatively so for all other
options. \JSM is very fast for accessing array with pre-computed
string indexes but slower for all other tests. We think that the test
shows the general good performance of our contribution \#1 presented
in Section~\ref{array-prop-names}. It is the fastest for the numerical
conversion (\textsf{foridx}) and consistently second for
\textsf{forin} and \textsf{forkey}. It is slightly slower for
\textsf{forstr} but for another reason. This test keeps allocating
small strings that have a very short life time. This pattern penalizes
the non-copying collector used by \hop.

%*---------------------------------------------------------------------*/
%*    Cache Misses                                                     */
%*---------------------------------------------------------------------*/
%%%
\ifcachemisses
\subsection{Performance of Cache Misses}
\label{perf:cache-misses}

For our last experiment, we measure the performance of cache misses
(see Section~\ref{caching-cache-misses} for the motivation of this
experiment). To measure the performance of cache misses, we follow
\citet{sf:cc19}'s methodology. We have used a simple micro benchmark
that repeatedly reads a property from objects that all have the same
shape:

\lstinputlisting[numbers=left, xleftmargin=0.38cm, language=hop, linerange={3-8,11-14,17-18}]{src/cache-hit.js}

\noindent For cache misses, the definition of \TT{os} is changed so no
object contains an \TT{x} property, which causes the function
\TT{readX} to miss at each read. The performance of each system is
presented in Figure~\ref{perf:cache-misses.fig}.

\begin{figure}[htb]
\centerline{\includegraphics[width=0.9\linewidth]{./cache.pdf}}
\caption{Comparing the performance of cache hits (left group) and
cache misses (right group). Smaller is better.
\label{perf:cache-misses.fig}}
\end{figure}

For such a simple test, some systems perform significantly better than
others because, for instance, they choose to aggressively unroll the
loop. This is not the important result here. The important feature is
the comparison of the cache-hit to the cache-miss executions. This
comparison tells us that the technique discussed in
Section~\ref{caching-cache-misses} enables \hop to eliminate almost
all of the extra cost of cache misses, as most JIT systems do, despite
\hop being an ahead-of-time compiler.
\fi
%%%
