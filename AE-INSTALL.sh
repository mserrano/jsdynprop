#!/bin/bash

PREFIX=/tmp/HOP
PATH=$PREFIX/bin:$PATH
export PATH

COMMIT=606404cafa1087db7e0e86eb2724387d7651028f

# === bigloo ================================================
biglootgz=bigloo-unstable.tar.gz

wget ftp://ftp-sop.inria.fr/indes/fp/Bigloo/$biglootgz -O /tmp/$biglootgz
bigloo=`tar tfz /tmp/$biglootgz | head -n 1`
pushd /tmp && rm -rf $bigloo && tar xvfz /tmp/$biglootgz && popd
(pushd /tmp/$bigloo && ./configure --prefix=$PREFIX && make -j 2 && make install && popd) || exit 1

# === hop ====================================================
/bin/rm -rf hop

git clone https://github.com/manuel-serrano/hop
(pushd hop && git checkout $COMMIT && ./configure --prefix=$PREFIX --disable-doc --license=academic --bigloo=$PREFIX/bin/bigloo && make && make install) || exit 1

# === jsbench ================================================
/bin/rm -rf jsbench

git clone https://github.com/manuel-serrano/jsbench

