Dynamic Property Caches -- Manuel Serrano and Robert Bruce Findler
==================================================================

This document explains how to reproduce the experimental results
presented in the paper. When possible, we STRONGLY encourage the
reviewers to use the native Linux version in order to use full
hardware features (included multi-core which is extensively used) but
we also provide a Docker image for simplifying the review process.


Linux
-----

Hop is implemented in Scheme using the Bigloo Scheme compiler. The latest
version of that compiler is needed. This compiler depends a the complete
Linux development environment (e.g., autoconf, automake, libtool, make, ...).
This Scheme environment also requires third party library that need to
be installed first (e.g, ssl, sqlite3, asound, flac, avahi, ...).

On a Debian based Linux system, all the prerequisite can be installed with:

  sudo apt-get -qq update
  sudo apt-get install -y dh-make
  sudo apt-get install -y libssl1.0.2
  sudo apt-get install -y libssl-dev
  sudo apt-get install -y libsqlite3-0
  sudo apt-get install -y libsqlite3-dev
  sudo apt-get install -y libasound2
  sudo apt-get install -y libasound2-dev
  sudo apt-get install -y libflac8
  sudo apt-get install -y libflac-dev
  sudo apt-get install -y libmpg123-0
  sudo apt-get install -y libmpg123-dev
  sudo apt-get install -y libavahi-core7
  sudo apt-get install -y libavahi-core-dev
  sudo apt-get install -y libavahi-common-dev
  sudo apt-get install -y libavahi-common3
  sudo apt-get install -y libavahi-client3
  sudo apt-get install -y libavahi-client-dev
  sudo apt-get install -y libunistring0
  sudo apt-get install -y libunistring-dev
  sudo apt-get install -y libpulse-dev
  sudo apt-get install -y libpulse0
  sudo apt-get install -y automake
  sudo apt-get install -y libtool
  sudo apt-get install -y libgmp-dev
  sudo apt-get install -y libgmp3-dev
  sudo apt-get install -y libgmp10

Once these package are installed, the artifact associated with the
"Dynamic Property Caches" can be compiled and executed with:

  ./AE-INSTALL.sh
  ./AE.sh


Docker
------

Build the docker image

  (cd docker; docker build -t jsdynprop .)

Run the docker image

  docker run jsdynprop /tmp/jsdynprop/AE.sh

This will compile and run all the benchmarks with Nodejs and Hop. The result
of this execution will be the latex table included in the paper appendix
(a simplified version as only Hop and Nodejs are tested here).

Run the full tests within the docker image (30 to 60 HOURS OF COMPUTATION):

  docker run jsdynprop /tmp/jsdynprop/AE.sh full

The generated PDF barchart file can be obtained with

  name=`docker ps -a --format "{{.Image}} {{.Names}}"  | grep jsdynprop | head -n 1 | awk '{print $2}'`
  docker cp $name:/tmp/jsdynprop/plain.pdf .


Windows (untested)
------------------

For installing and running Hop under windows, enable the Linux Shell
under Windows and proceed as far a Debian distribution.


MacOS
-----

The requisite for MacOS depends on the local installation (Macports or
Brew). Check the hop/INSTALL.md file 

  https://github.com/manuel-serrano/hop/blob/master/INSTALL.md

and execute

  ./AE-INSTALL.sh
  ./AE.sh
