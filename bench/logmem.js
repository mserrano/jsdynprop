/*=====================================================================*/
/*    serrano/diffusion/article/jsdynprop/bench/logmem.js              */
/*    -------------------------------------------------------------    */
/*    Author      :  Manuel Serrano                                    */
/*    Creation    :  Tue Oct 15 11:52:56 2019                          */
/*    Last change :  Tue Oct 15 12:57:59 2019 (serrano)                */
/*    Copyright   :  2019 Manuel Serrano                               */
/*    -------------------------------------------------------------    */
/*    This program adds a memory comparison entry to a jsbench log     */
/*    file. It reads the original file. Extract the benchmark name.    */
/*    Read the memory profiling information, and add a new dummy       */
/*    engine entry to the log file.                                    */
/*=====================================================================*/
"use hopscript";

const path = require( "path" );

const logs = require( "../" + process.argv[ 2 ] );
const log = logs[ 0 ];

// get the benchmark path
const bench = path.basename( log.path, ".js" );
const engines = log.engines;
const mem = require( "./LOGS/" + bench + ".mem.json" );

const ratio = mem.hop / mem.noscache;
	 
engines[ 0 ].name = "time";

engines.push( { "name": "mem", 
	       "logs": [ { "times": { "ustimes": [ratio], "rtimes": [ratio] },
			   "time": ratio } ] } );

console.log( JSON.stringify( logs ) );
				    
			   
