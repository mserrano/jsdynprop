"use strict";

var names = [];

function Cons( a, d ) {
   this[ names[ 0 ] ] = a;
   this[ names[ 1 ] ] = d;
}

function interval( min, max ) {
   if( min > max ) {
      return null;
   } else {
      return new Cons( min, interval( min + 1, max ) );
   }
}

function sfilter( p, l ) {
   if( l === null ) {
      return l;
   } else {
      let a = l[names[0]];
      let r = l[names[1]];

      if( p( a ) ) {
	 return new Cons( a, sfilter( p, r ) );
      } else {
	 return sfilter( p, r );
      }
   }
}

function remove_multiples_of( n, l ) {
   return sfilter( m => (m % n) != 0, l );
}

function sieve( max ) {
   function filter_again( l ) {
      if( l === null ) {
	 return l;
      } else {
	 let n = l[names[0]];
	 let r = l[names[1]];

	 if( n * n > max ) {
	    return l;
	 } else {
	    return new Cons( n, filter_again( remove_multiples_of( n, r ) ) );
	 }
      }
   }
   return filter_again( interval( 2, max ) );
}

function do_list( f, lst ) {
   while( lst !== null ) {
      f( lst[names[0]] );
      lst = lst[names[1]];
   }
}

function length( lst ) {
   let res = 0;

   while( lst != null ) {
      res++;
      lst = lst[names[1]];
   }

   return res;
}

function doit( num ) {
   let res = 0;
   
   while( num-- > 0 ) {
      names = ["CAR", "CDR"];
      res += length( sieve( 3000 ) );
   }

   return res;
}

function list2array( lst ) {
   let len = length( lst );
   var res = new Array( len );

   for( let i = 0; i < len; lst = lst[names[1]], i++ ) {
      res[ i ] = lst[names[0]];
   }

   return res;
}
      
const expected_result = [
   2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31,
   37, 41, 43, 47, 53, 59, 61, 67, 71, 73,
   79, 83, 89, 97 ];

function main() {
   names = ["car", "cdr"];
   let s100 = sieve( 100 );
   let n = 4000;

   doit( n );
   names = ["car", "cdr"];
   console.log( list2array( s100 ) );
}


main();


