"use strict"

// test for-in used with arrays
function foo( a, k ) {
   let r = 0;
   const l = k.length;
	    
   for( let i = 0; i < l; i++ ) {
      r += a[ k[ i ] ];
   }
   return r;
}

function t() { 
   let a = [2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2];
   let k = [];
   let r;
   
   for( let i in a ) {
      k.push( i );
   }
   
   for( let i = 0; i < 4000000; i++ ) {
      r = foo( a, k );
   }
   
   return r;
}
	  
console.log( "run=", t() );
   
