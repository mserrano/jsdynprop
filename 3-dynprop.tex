%*=====================================================================*/
%*    serrano/diffusion/article/jsdynprop/3-dynprop.tex                */
%*    -------------------------------------------------------------    */
%*    Author      :  Manuel Serrano                                    */
%*    Creation    :  Wed Aug  7 12:18:25 2019                          */
%*    Last change :  Thu Jan  9 16:07:49 2020 (serrano)                */
%*    Copyright   :  2019-20 Manuel Serrano                            */
%*    -------------------------------------------------------------    */
%*    Dynamic properties                                               */
%*=====================================================================*/

%*---------------------------------------------------------------------*/
%*    dynamic properties                                               */
%*---------------------------------------------------------------------*/
\section{Dynamic Properties}
\label{dynprop}

As presented in the introduction, techniques for implementing
efficient object accesses are well understood and deployed in
popular compilers. However, they demand that the property name is
constant, and thus do not apply when the property name is a
dynamic value. In this section, we present the two techniques we have
developed to mitigate that problem. The first one applies to array
accesses and the second one to regular object accesses.

\subsection{Array Property Names}
\label{array-prop-names}

\js arrays are complex to implement efficiently as they
support many dynamic operations. They can be dynamically
extended or shrunk, they can be sparse, and more importantly,
accessing arrays elements obeys the same semantics as
object accesses. That is, the index has to be converted into
a string value to look it up in the array and, if the index does
not have a value, the array's prototype
chain must be consulted.

\js arrays are used so extensively that all implementations try
their best to implement them efficiently by using as much as possible
flat sequences of values indexed by small
integers~\cite{serrano:dls18}. Unfortunately, these efficient
techniques are not always applicable. A typical pattern that defeats
these optimization is the \TT{for..in} loop. Consider this example:

\lstinputlisting[language=hop]{src/forinarr.js}

\noindent Let us assume that \TT{a} is an array. The semantics tells
us that the local variable \TT{i} must be successively bound to all
property names of the array elements, \emph{i.e.,} all the indexes
of the elements that the array contains, but where the indices are represented as strings.
Thus, in
the expression \TT{i + " is " + a[i] + " "} the variable \TT{i} must
be a string value. Because of the first argument to \TT{+}, this
string is used to form the global result and because of the third
(\TT{a[i]}), it is also used to access the
array. The semantics thus defeats the simple-minded strategy that represents an
array as a linear sequence of memory, for two reasons:

\begin{enumerate}

\item Since the array indices are not stored in the object itself, the
strings representing the indices must be allocated at runtime.

\item Because \TT{i} must be a string, the conversion to an integer
  would naturally be handled at runtime. Beyond the cost of parsing
  the string, the conversion to an integer may even fail as an array
  may have properties that are not its indicies (\emph{e.g.,} its length).

\end{enumerate}

Problem \#1 is similar to the problem of boxing numbers. The compiler
folklore tells us that it can be mostly solved by pre-allocating small
indexes~\cite{ss:icfp02}. To avoid the string parsing of problem \#2,
the second solution is to store, inside the string itself, the
corresponding index.

\noindent \contribution{1} We extend the class hierarchy of
Figure~\ref{ropes-fig} with an additional class for indexes (see
Figure~\ref{ropes-index-fig}). In addition to the string itself,
instances of that subclass also store the corresponding integer
index.\footnote{Examining the source code of other
engines~\cite{mozilla-indexes, v8-indexes} suggests that they also
store the index inside the string, but our performance analysis
suggests that the situation is subtle; we explore it
in Section~\ref{perf:dynamic-property-name}.}
Additionally, to avoid dynamic memory allocation for indices, \hop
pre-allocates the first $N$ array indices, where $N$ is a global
parameter of the system. If needed, the array of pre-allocated indices
is extended dynamically.

\begin{figure}[htb]
\centerline{\includegraphics[width=0.50\linewidth]{./svg/index.pdf}}
\caption{String indices are instances of a dedicated class to
avoid expansive string parsing.
\label{ropes-index-fig}} \end{figure}

This simple framework is memory efficient as indexes are pre-allocated
in a memory area that do not even need to be scanned by the garbage
collector. It enables optimal conversions from number to string and
vice versa. As an index is also a regular rope, strings operations
such as concatenation do not need any modification and preserve their
efficiency. We compare the performance of this encoding to those used in other
systems in Section~\ref{perf:dynamic-property-name}
(Figure~\ref{perf:array-property-name.fig}).  We show that it
enables \hop to be \under{3$\times$ to 5$\times$ faster} than all
other tested systems when proxied arrays are used (see
Figure~\ref{perf:proxy.fig}).

\subsection{Dynamic Property Accesses}
\label{dyn-prop-access}

When compiling an expression \TT{obj[prop]}, the inline cache
technique applies only if \TT{prop} is a constant. A naive attempt to
improve the cache would simply convert the given property name and
compare it:

\lstinputlisting[numbers=none, language=C]{src/idyncache.c}

\noindent but this is ineffective because \TT{prop} is likely
to change frequently when it is not a literal string. For instance,
consider the \TT{for..in} loop from
Section~\ref{array-prop-names}. Inside the loop, the value of \TT{i}
changes at each iteration and thus the naive extension will always miss the
cache. The solution we propose is the following:

\noindent \contribution{2} 
\emph{When the property name is dynamic, the cache is attached to the
string value itself, instead of to the program location.}

This requires a minor modification to the implementation of string
values as they must have extra slots to store the read and write
cache. In practice the \texttt{right} property and one of the
\texttt{rcache} or \texttt{pcache} can be merged as property names are
always normalized ropes, for which only the \texttt{left} property is
used. In Section~\ref{perf:string} we evaluate the impact of adding
extra fields to the string base implementation. We show that for most
tests, the negative impact is invisible. In the worst cases, we have
found that it increases memory allocation by 8\% and it degrades
performance by less than 3\% (see Figure~\ref{perf:noscache.fig}).
Note that regular strings do not need to carry caches. Only strings
representing property names do. So, it might be doable to use a more
complex class hierarchy than this of Figure~\ref{ropes-dyn-fig} where
cache attributes are declared only in the subclasses used for
property names.

\begin{figure}[htb]
\centerline{\includegraphics[width=0.50\linewidth]{./svg/dynprop.pdf}}
\caption{The complete class hierarchy for \js string values with the
additional attributes for handling string hashing and string caches.
Note that the write cache \texttt{wcache} and the rope right-and-side
part are merged.
\label{ropes-dyn-fig}}
\end{figure}

The sequence of accessing a dynamic property is adapted from the
conventional inline cache sequence from Section~\ref{intro}:

\lstinputlisting[numbers=none,language=C]{src/dyncache.c}

We have implemented this technique in \hop and we have evaluated its
performance using two micro-benchmarks that are presented in
Section~\ref{perf:dynamic-property-name}. It establishes that this
idea, although simple and easy to deploy, outperforms popular
industrial \js implementations. The experimental report also shows
that with this modification, \hop is the only system that imposes
\under{no significant performance overhead} for dynamic property
names. We conjecture that is also the main reason why \hop outperforms
the other systems by a factor of up to \under{10$\times$ to
100$\times$} when testing more general proxy programs
(see Section~\ref{perf:proxy}).
